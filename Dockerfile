# syntax=docker/dockerfile:1

FROM docker.io/library/rust:1-alpine3.20 AS builder

RUN apk update
RUN apk add git pkgconfig openssl-dev musl-dev make perl

WORKDIR /opt/rust-synapse-tools/
COPY . .

ENV RUSTFLAGS="-C target-feature=-crt-static"

# arm64 builds consume a lot of memory if `CARGO_NET_GIT_FETCH_WITH_CLI` is not
# set to true, so we expose it as a build-arg.
ARG CARGO_NET_GIT_FETCH_WITH_CLI=false
ENV CARGO_NET_GIT_FETCH_WITH_CLI=$CARGO_NET_GIT_FETCH_WITH_CLI
ARG BUILD_PROFILE=release
ENV BUILD_PROFILE=$BUILD_PROFILE

WORKDIR /opt/rust-synapse-tools/synapse-find-unreferenced-state-groups
RUN cargo build --profile=$BUILD_PROFILE

WORKDIR /opt/rust-synapse-tools/rust-synapse-compress-state
RUN cargo build --profile=$BUILD_PROFILE  --no-default-features --features clap

WORKDIR /opt/rust-synapse-tools/rust-synapse-compress-state/synapse_auto_compressor/
RUN cargo build  --profile=$BUILD_PROFILE --no-default-features --features clap

FROM docker.io/library/alpine:3.20

RUN apk add --no-cache libgcc

COPY --from=builder /opt/rust-synapse-tools/synapse-find-unreferenced-state-groups/target/*/rust-synapse-find-unreferenced-state-groups /usr/local/bin/rust-synapse-find-unreferenced-state-groups
COPY --from=builder /opt/rust-synapse-tools/rust-synapse-compress-state/target/*/synapse_compress_state /usr/local/bin/synapse_compress_state
COPY --from=builder /opt/rust-synapse-tools/rust-synapse-compress-state/target/*/synapse_auto_compressor /usr/local/bin/synapse_auto_compressor
