#!/bin/sh

(cd ../synapse-find-unreferenced-state-groups; git diff master..pgpassfile) > synapse-find-unreferenced-state-groups.diff
(cd ../synapse-find-unreferenced-state-groups; git rev-parse master) > synapse-find-unreferenced-state-groups.sha
(cd ../rust-synapse-compress-state; git diff main..pgpassfile) > rust-synapse-compress-state.diff
(cd ../rust-synapse-compress-state; git rev-parse main) > rust-synapse-compress-state.sha
